function handleFileSelect(evt) {
    var file = evt.target.files[0]; // FileList object
	var cleanFileHTML = document.getElementById('files').innerHTML;
    // files is a FileList of File objects. List some properties.
    var output;
    //for (var i = 0, f; f = files[i]; i++) {
      if (file.type == "video/mp4") {
      	output = '<strong>' + file.name + '</strong>, File Size is ' + file.size;
      } else {
      	document.getElementById('files').innerHTML = cleanFileHTML;
      	output = 'You can only upload <strong>mp4</strong> video file.';
      }
      document.getElementById('list').innerHTML = '<ul>' + output + '</ul>';
  }
